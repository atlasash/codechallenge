<?php


class cart 
{

	public $cart = array(
		"cartItems" => array(),
		"total" => 0,
		"items" => 0

	);


	public function addToCart ($cartItem) {
		if (!$cartItem) {
			return;
		}
		$cart = $this->cart;
		$cart['cartItems'][] = $cartItem;
		$qty = $cartItem['qty'];
		$total = $cart['total'] + $cartItem['price']*$qty;
		$items = $cart['items'] + 1;
		$cart['total'] = round($total, 2);
		$cart['items'] = $items;
		$this->cart = $cart;
	}

	public function getTotal ($filters = array()) {
		$cart = $this->cart;
		$cartItems = $cart['cartItems'];
		$total = $cart['total'];
		if ($filters) {
			$total = 0;
			foreach ($cartItems as $cartItem) {
				if (in_array($cartItem['id'], $filters) || in_array($cartItem['parent'], $filters)) {
					$total += $cartItem['price']*$cartItem['qty'];
				}
			}
			$total = round($total, 2);
		}
		return $total;
	}

}
