<?php

/**
	Base Class for all electronics
*/
class ElectronicItem 
{

	protected $itemId = 0;
	protected $itemType = 0;
	protected $extras = array();
	protected $limitExtras = 0;
	protected $extrasEnabled = 1;

	//all availables items (should be moved to Database), 
	protected $items = array(
		"1" => array(
			"item" => "Samsung TV",
			"itemType" => 1,
			"price" => 599.99
		),
		"2" => array(
			"item" => "LG TV",
			"itemType" => 1,
			"price" => 499.99
		),
		"3" => array(
			"item" => "Remote Controller",
			"itemType" => 2,
			"price" => 39.99
		),
		"4" => array(
			"item" => "Wired Controller",
			"itemType" => 2,
			"price" => 29.99
		),
		"5" => array(
			"item" => "Samsung Console",
			"itemType" => 3,
			"price" => 129.99
		),
		"6" => array(
			"item" => "GE Microwave",
			"itemType" => 4,
			"price" => 199.99
		)
	);

	//all availables items (should be moved to Database), 
	protected $itemTypes = array(
		"1" => array(
			"type" => "television"
		),
		"2" => array(
			"type" => "controller"
		),
		"3" => array(
			"type" => "console"
		),
		"4" => array(
			"type" => "microwave"
		)
	);

	/**
		Check if limit extras has been reached
		@return bool
	*/
	function maxExtras()
	{
		$overLimit = false;
		
	    if ($this->limitExtras && (count($this->extras) >= $this->limitExtras)) {
	    	$overLimit = true;
	    }

	    return $overLimit;
	}

	/**
		Attach an item to another item if allowed
		@param $itemId
		@return array
	*/
	function attachItem($itemId, $qty=1) {
		$cartItem =array();

		if ($this->maxExtras() || !$this->extrasEnabled) {
			return $cartItem;
		}

		if (!isset($this->items[$itemId])) {
			return $cartItem;
		}

		$this->extras[] = $itemId;
		$cartItem = array(
			"id" => $itemId,
			"item" => $this->items[$itemId]['item']." (attached to ".$this->items[$this->itemId]['item'].")",
			"parent" => $this->itemId,
			"price" => $this->items[$itemId]['price'],
			"itemType" => $this->items[$itemId]['itemType'],
			"qty" => $qty
		);

		return $cartItem;
	}


	/**
		Get single item array to add item to cart
		@param $qty
		@return array
	*/
	function getCartItem($qty=1) {
		$cartItem = array();

		if ($this->itemId) {
			$cartItem = array(
				"id" => $this->itemId,
				"item" => $this->items[$this->itemId]['item'],
				"parent" => 0,
				"price" => $this->items[$this->itemId]['price'],
				"itemType" => $this->items[$this->itemId]['itemType'],
				"qty" => $qty
			);
		}

		return $cartItem;
	}

	/**
		Set the itemId of the object, confirming the itemId can be used with the child class
		@param $id
	*/
	function setItemId ($id) {
		
		if (!isset($this->items[$id])) {
			return;
		}

		if ($this->items[$id]['itemType'] != $this->itemType) {
			return;
		}

		$this->itemId = $id;
	}

	function getItemId () {
		return $this->itemId;
	}

}
