<?php

$models = glob(dirname(__FILE__).'/models/*.php');
foreach ($models as $model) {
    require $model;
}

$cart = new cart();

//add console and attach controllers
$console1 = new console(5);
$cart->addToCart($console1->getCartItem());
$cart->addToCart($console1->attachItem(3,2));
$cart->addToCart($console1->attachItem(4,2));

//add tv1 and controllers
$tv1 = new television(1);
$cart->addToCart($tv1->getCartItem());
$cart->addToCart($tv1->attachItem(3,2));

//add tv2 and controller
$tv2 = new television(2);
$cart->addToCart($tv2->getCartItem());
$cart->addToCart($tv2->attachItem(3));

//add microwave
$microwave1 = new microwave(6);
$cart->addToCart($microwave1->getCartItem());


$cartItems = $cart->cart['cartItems'];

$purchase_items = array();
foreach ($cartItems as $item) {
	$purchase_items[$item['item']."({$item['qty']})"] = round($item['price']*$item['qty'], 2);
}
asort($purchase_items);


echo "Your cart:\n";
foreach ($purchase_items as $item => $price) {
		echo $item.": ".$price."\n";
}
echo "\nYour total is : ".$cart->getTotal()."\n";
